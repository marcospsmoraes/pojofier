package com.vconrado.pojofier.pojo;

import com.vconrado.pojofier.annotation.PojoField;
import com.vconrado.pojofier.converter.EventToDelayConverter;

public class EventPojo {

    Long id;
    String name;
    @PojoField(value = PojoField.SOURCE_OBJECT, converter = EventToDelayConverter.class)
    Long delay;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDelay() {
        return delay;
    }

    public void setDelay(Long delay) {
        this.delay = delay;
    }

    @Override
    public String toString() {
        return "EventPojo{" + "id=" + id + ", name=" + name + ", delay=" + delay + '}';
    }

}
