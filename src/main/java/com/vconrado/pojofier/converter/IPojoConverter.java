package com.vconrado.pojofier.converter;

/**
 *
 * @author Vitor Gomes
 * @param <P>
 * @param <R>
 */
public interface IPojoConverter<P, R> {

    R convert(P p);

}
