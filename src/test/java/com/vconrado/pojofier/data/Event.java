package com.vconrado.pojofier.data;

import java.util.Date;

/*
 *  *****************************************************************************
 *                    Lean Survey Pesquisas de Mercado S.A
 *  -----------------------------------------------------------------------------
 http://www.leansurvey.com.br
 *                      Contato: <dev@leansurvey.com.br>
 *  *****************************************************************************
 */
public class Event {

    Long id;
    String name;
    Date start;
    Date end;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

}
