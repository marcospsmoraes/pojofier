package com.vconrado.pojofier;

import java.util.List;

/*
 *  *****************************************************************************
 *                    Lean Survey Pesquisas de Mercado S.A
 *  -----------------------------------------------------------------------------
 http://www.leansurvey.com.br
 *                      Contato: <dev@leansurvey.com.br>
 *  *****************************************************************************
 */
public class PojofierWrapper {

    public <T> List<T> pojify(Class<T> to, List<?> fromList) {
        List<T> ret = Pojofier.pojify(to, fromList);
        return ret;
    }

    public <T> T pojify(Class<T> to, Object from) {
        T ret = Pojofier.pojify(to, from);
        return ret;
    }

    public PojofierWrapper addResource(String key, Object o) {
        PojofierWrapper pw = Pojofier.addResource(key, o);
        return pw;
    }

}
