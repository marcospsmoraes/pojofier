package com.vconrado.pojofier;

import com.vconrado.pojofier.annotation.IfNullReturnNull;
import com.vconrado.pojofier.annotation.PojoField;
import com.vconrado.pojofier.annotation.PojoIgnore;
import com.vconrado.pojofier.converter.IPojoConverter;
import com.vconrado.pojofier.converter.PojoFieldConverter;
import com.vconrado.pojofier.exception.PojofierException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Vitor Gomes
 */
public class Pojofier implements Serializable {

    private final static PojofierWrapper INSTANCE = new PojofierWrapper();
    private static Map<String, Object> context = new HashMap();

    public static PojofierWrapper addResource(String key, Object o) {
        Pojofier.context.put(key, o);
        return INSTANCE;
    }

    public static Object getResource(String key) {
        return Pojofier.context.get(key);
    }

    public static <T> List<T> pojify(Class<T> to, List<?> fromList) {
        List<T> list = new ArrayList<>();
        for (Object o : fromList) {
            list.add(Pojofier.pojify(to, o));
        }
        return list;
    }

    private void fooMethod() {

    }

    public static <T> T pojify(Class<T> to, Object from) {
        T pojo;
        if (from == null) {
            IfNullReturnNull inrn = to.getAnnotation(IfNullReturnNull.class);
            if (inrn != null) {
                return null;
            }
            try {
                return to.newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
                System.err.println("Erro ao instanciar objeto a ser retornado (null case): " + ex.getMessage());
                return null;
            }
        }
        try {
            pojo = to.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            System.err.println("Erro ao instanciar objeto a ser retornado: " + ex.getMessage());
            return null;
        }
        for (Field toField : to.getDeclaredFields()) {
            if (toField.getAnnotation(PojoIgnore.class) == null) {
                try {
                    Method toSetMethod = obtainSetMethod(toField, to);
                    Method fromGetMethod = obtaionGetMethod(toField, toSetMethod, from);
                    // fromGetMethod pode ser null. Neste caso, passe o objeto inteiro
                    invokeMethod(toField, from, fromGetMethod, pojo, toSetMethod);
                } catch (PojofierException ex) {
                    System.out.println("Falha ao pojificar");
                    ex.printStackTrace();
                    return null;
                }
            }
        }
        return to.cast(pojo);
    }

    private static <T> void invokeMethod(Field toField, Object from, Method fromGetMethod, T pojo, Method toSetMethod) throws PojofierException {
        // verifica se o tipo esperado é igual ao valor
        if (toSetMethod.getParameterCount() != 1) {
            throw new PojofierException("'" + toSetMethod.getName() + "' method must have only one parameter.");
        }
        PojoField pojoFieldAnnotation = toField.getAnnotation(PojoField.class);
        IPojoConverter pojoFieldConverter = new PojoFieldConverter();
        if (pojoFieldAnnotation != null) {
            try {
                pojoFieldConverter = pojoFieldAnnotation.converter().newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
                throw new PojofierException("Falha ao instanciar o converter");
            }
        }
        try {

            Object value = (fromGetMethod != null) ? fromGetMethod.invoke(from) : from;
            if (value != null && Collection.class.isAssignableFrom(value.getClass())) {
                Collection valueCollection = (Collection) value;
                Collection convertedValueCollection = (Collection) value.getClass().newInstance();
                for (Object item : valueCollection) {
                    convertedValueCollection.add(pojoFieldConverter.convert(item));
                }
                toSetMethod.invoke(pojo, convertedValueCollection);
            } else {
                toSetMethod.invoke(pojo, pojoFieldConverter.convert(value));
            }
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException | NullPointerException ex) {
            throw new PojofierException("Erro ao processar pojofy (toField: " + toField.getName() + ") " + ex.getMessage());
        }
    }

    private static Method obtainSetMethod(Field toField, Class<?> to) throws PojofierException {
        String setMethodName = setGettfyField("set", toField.getName());
        Method toSetMethod = null;
        try {
            toSetMethod = to.getMethod(setMethodName, toField.getType());

        } catch (NoSuchMethodException ex) {
            throw new PojofierException(
                    "Could not identify the method '" + setMethodName + "' for setting the field '"
                    + toField.getName() + "' with parameter type '" + toField.getType().getName() + "'");
        }
        return toSetMethod;
    }

    private static Method obtaionGetMethod(Field toField, Method toSetMethod, Object from) throws PojofierException {
        PojoField pojoField = toField.getAnnotation(PojoField.class);
        String fromGetMethodName;
        if (pojoField != null && pojoField.value().length() > 0) {
            String fromFieldName = pojoField.value();
            if (PojoField.SOURCE_OBJECT.equals(fromFieldName)) {
                return null;
            }
            fromGetMethodName = setGettfyField("get", fromFieldName);
        } else {
            fromGetMethodName = toSetMethod.getName().replace("set", "get");
        }
        try {
            return from.getClass().getMethod(fromGetMethodName);
        } catch (NoSuchMethodException ex) {
            throw new PojofierException(
                    "Could not identify the method '" + fromGetMethodName
                    + "' to get the value for the field: '" + toField.getName() + "'."
            );
        }
    }

    private static String setGettfyField(String prefix, String fieldName) {
        return prefix + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    }

    public static void clearContext() {
        Pojofier.context.clear();
    }

    public static void listContext() {
        for (Object key : Pojofier.context.keySet()) {
            System.out.println("Key: " + key);
        }
    }
}
