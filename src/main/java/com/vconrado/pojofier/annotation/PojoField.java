package com.vconrado.pojofier.annotation;

import com.vconrado.pojofier.converter.IPojoConverter;
import com.vconrado.pojofier.converter.PojoFieldConverter;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Vitor Gomes
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PojoField {

    String value() default "";
    public static String SOURCE_OBJECT = "#";

    Class<? extends IPojoConverter> converter() default PojoFieldConverter.class;
}
